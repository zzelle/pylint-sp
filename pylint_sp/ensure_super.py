
from astroid import InferenceError, nodes
from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker, utils
from pylint.checkers.stdlib import OPEN_MODULE

MSGS = {
    'W4203': ('super must be called',
              'super-must-be-called',
              'Used to ensure super() is called in overloaded methods'),
    }


class SuperCalledChecker(BaseChecker):

    __implements__ = (IAstroidChecker,)

    name = 'super-called'
    msgs = MSGS
    priority = -1
    
    
    options = (('ensure-super-for',
                {'default': (),
                 'type': 'csv',
                 'metavar': '<ensure-super>',
                 'help': 'List of Class.method for which super must be called in inheritated classes',
             }),)

    def __init__(self, linter=None):
        super(SuperCalledChecker, self).__init__(linter)
        self.should_check = []

    def leave_function(self, node):
        if node not in self.should_check:
            return
        self.add_message('super-must-be-called', node=node)
        self.should_check.remove(node)
        
    def visit_function(self, node):
        if not node.is_method():
            return
        for cls in node.parent.mro()[1:]:
            qname = '{}.{}'.format(cls.qname(), node.name) 
            if any(qname.endswith(mustcheck) for mustcheck in self.config.ensure_super_for):
                break
        else:
            return
        self.should_check.append(node)

    def visit_callfunc(self, node):
        if node.scope() not in self.should_check:
            return
        if not hasattr(node.func, 'name') or node.func.name != 'super':
            return
        self.should_check.remove(node.scope())

def register(linter):
    linter.register_checker(SuperCalledChecker(linter))
