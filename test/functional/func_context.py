# pylint: disable=missing-docstring,invalid-name

from threading import Lock

f = file('foo')  # [file-without-context-manager]

with file('bar') as f:
    pass

def do_stuff():
    pass

l = Lock()
l.acquire()  # [lock-without-context-manager]
do_stuff()
l.release()  # [lock-without-context-manager]

with l:
    do_stuff()
