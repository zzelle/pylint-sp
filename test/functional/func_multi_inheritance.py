# pylint: disable=blacklisted-name,invalid-name,missing-docstring,missing-module-attribute,too-few-public-methods,super-init-not-called

class A(object):
    def __init__(self):
        pass


class MixIn(object):
    def toto(self):
        pass


class B(A, MixIn):
    def __init__(self):
        pass


class C(B):
    pass


class D(A, B):  # [safepython-multiple-inheritance]
    pass
