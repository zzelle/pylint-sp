class A(object):
    def must(self):
        pass
    def not_must(self):
        pass


class B(A):
    def must(self): #[super-must-be-called]
        pass
    def not_must(self):
        pass
    
class G(A):
    def must(self):
        super(G, self).must()

